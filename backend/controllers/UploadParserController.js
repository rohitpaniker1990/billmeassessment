var express = require('express');
var router = express.Router();
const multer = require('multer');
var path = require('path');
var lineReader = require('reverse-line-reader');



const uploadPath = path.dirname(require.main.filename)+'/../uploads';


exports.uploadBill = (req, res) => {
  var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, uploadPath);
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
    }
  });
  
  var upload = multer({ storage });

  const format = ['Total :', 'Grand Total:', 'Tendered­AMT :'];
  
  const singleUpload = upload.single('mBill');

  singleUpload(req, res, function(err, resp) {
    if(err) {
      return res.status(400).send(err);
    }

    const file = req.file;
    var amount = 0;
    var regex = /[\d|,|.|e|E|\+]+/g;
    if (!file) {
        const error = new Error('Please upload a file')
        error.httpStatusCode = 400
        return next(error)
      }

      lineReader.eachLine(uploadPath+'/'+file.filename, function(txtLine, last) {
          var result = [];
        //   console.log('txtLine', txtLine);
        for(i = 0; i < format.length; i++) {
            if(txtLine.search(format[i]) !== -1) {
                result = txtLine.match(regex);
                amount = result[result.length-1];
                console.log('Amount is', amount);
                res.status(200).send({'Final Amount Is': amount});
                return false;
            }
        };
      });
  });
}