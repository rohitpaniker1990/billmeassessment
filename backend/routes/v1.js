var express = require('express');
var router = express.Router();

var uploadParserController = require('./../controllers/UploadParserController');


router.post('/:action', handlePOST);

function handlePOST(req, res) {
    switch(req.params.action) {
        case "uploadbill":
            uploadParserController.uploadBill(req, res);
            break;
        default:
            break;
    }
}

module.exports = router;
